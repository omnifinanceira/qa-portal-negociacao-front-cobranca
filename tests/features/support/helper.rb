require 'allure-cucumber'

module Helper

   def tirar_foto(nome_arquivo, resultado)
      caminho_arquivo = "results/screenshots/teste_#{resultado}"
      foto = "#{caminho_arquivo}/#{nome_arquivo}.png"
      attach(page.save_screenshot(foto), 'image/png')

      Allure.add_attachment(
         name: "Attachment",
         source: File.open("#{foto}"),
         type: Allure::ContentType::PNG,
         test_case: true
      ) 
   end

   ### EM 03/11/2022 IMPLEMENTADO ESTE MÉTODO MAS FOI COMENTADO POIS NÃO FUNCIONOU.
   def tirar_foto_allure(nome_arquivo, resultado)
      caminho_arquivo_1 = "C:/results/screenshots/teste_#{resultado}"
      foto = "#{caminho_arquivo_1}/#{nome_arquivo}_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.png"
      
      caminho_arquivo = "C:/results/screenshots"
      foto = "#{caminho_arquivo}/#{nome_arquivo}_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.png"

      attach(page.save_screenshot(foto), 'image/png')
      Allure.add_attachment(
         name: "Attachment",
         source: File.open("#{foto}"),
         type: Allure::ContentType::PNG,
         test_case: true
      ) 
  end

end