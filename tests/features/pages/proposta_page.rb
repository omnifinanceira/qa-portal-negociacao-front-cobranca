class PropostaPage < SitePrism::Page

    elements :card_divida, :xpath, "//div[@class='card-divida-main']"
    element :botao_proximo, 'button[class="card-button button-modified"]'
    elements :botao_proximo, 'button[class="acerto-button card-form-element"]'
    element :botao_yes, 'button[class="answer answer-yes"]'
    element :botao_no, 'button[class="answer answer-no"]'
    #elements :texto_confirmacao_a_vista, :xpath, "//p[contains(text(),'Você vai pagar à vista o valor de:')]"
    elements :texto_confirmacao_a_vista, :xpath, "//p[contains(text(),'Pagamento à vista')]"
    elements :texto_confirmacao_parcelado, :xpath, "//p[contains(text(),'Total:')]"
    elements :texto_mais_informacoes, :xpath, "//p[contains(text(),'Precisa de ajuda?')]"
    elements :texto_precisa_ajuda, :xpath, "//h1[contains(text(),'Precisa de ajuda?')]"
    elements :texto_sem_divida_atraso, :xpath, "//h4[contains(text(),'Você não possui dívidas em atraso')]"
    elements :texto_parcelas_em_atraso, :xpath, "//p[contains(text(),'Parcelas em atraso')]"
    #elements :texto_proposta_ruim, :xpath, "//p[contains(text(),'Humm, você está chegando perto.')]"
    elements :texto_resumo_proposta, :xpath, "//h1[contains(text(),'Resumo da proposta')]"
    elements :texto_resumo_acordo, :xpath, "//h1[contains(text(),'Resumo do acordo')]"
    elements :texto_proposta_boa, :xpath, "//p[contains(text(),'Uhul! Sua proposta está perfeita!')]"
    elements :texto_divida_negociada, :xpath, "//span[contains(text(),'Dívida negociada')]"
    elements :mensagem_temporaria, :xpath, "//div[@class='temporary-message']/p[@class='message']"
    elements :texto_proposta_recusada, :xpath, "//h1[contains(text(),'Infelizmente a sua proposta foi recusada.')]"
    elements :detalhes_da_divida, :xpath, "//h3[contains(text(),'Detalhes da dívida')]"
    elements :proposta_pre_aprovada, :xpath, "//div[contains(text(),'Nossa proposta:')]"
    #elements :clicar_card, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-home-negotiation/div/section/omnids-card-debt-pn01/div/omni-card/div/div[2]/div/div[1]"
    elements :clicar_card_cliente, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-home-negotiation/div/section/omnids-card-debt-pn01/div/omni-card/div/div[2]/div/div[1]"
    element :seleciona_data_1, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step3-due-date/section/div/omnids-card-date-payment[1]"
    element :seleciona_data_2, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step3-due-date/section/div/omnids-card-date-payment[2]"
    element :seleciona_data_3, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step3-due-date/section/div/omnids-card-date-payment[3]"
    element :seleciona_data_4, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step3-due-date/section/div/omnids-card-date-payment[4]"
    element :seleciona_data_5, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step3-due-date/section/div/omnids-card-date-payment[5]"
    element :seleciona_parcela_1, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step2-installment/section/div/ul/li[1]/omnids-installment-value01/div/div/p[1]/span[1]/span"
    element :seleciona_parcela_2, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step2-installment/section/div/ul/li[2]/omnids-installment-value01/div/div/p[1]/span[1]/span"
    element :seleciona_parcela_3, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step2-installment/section/div/ul/li[3]/omnids-installment-value01/div/div/p[1]/span[1]/span"
    element :seleciona_parcela_4, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step2-installment/section/div/ul/li[4]/omnids-installment-value01/div/div/p[1]/span[1]/span"
    element :clicar_detalhes_divida, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-detail-debit-negotiated/section/div/div[1]/div[2]/p"
    #element :clicar_mais_informacoes, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-container-detail-debit/app-detail-debit/div/div[2]/p[2]"
    element :clicar_mais_informacoes, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-contract-id/section/app-detail-debit/div/div[2]/p[2]"
    element :clicar_fechar_tela_x, :xpath, "//*[@id='cdk-overlay-1']/app-debt-details-more-info/div/div[1]"
    element :clicar_seta_voltar, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/app-navbar-negotiation-web/div/div"
    element :clicar_nao_cabe_meu_bolso, :xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-offer-proposal-other/div/div/div[4]/div"

    def initialize
        @classe_login = LoginPage.new
        @util = Util.new
    end

    def aguarda_dividas
        $wait.until{card_divida}
        sleep(20)
    end

    def aguarda_detalhes_divida
        #binding.pry
        $wait.until{detalhes_da_divida}
        sleep(15)
    end

    def aguarda_proposta_pre_aprovada
        $wait.until{proposta_pre_aprovada}
        sleep(20)
    end

    def simula(simular)
        @classe_login.busca_botao(simular)
    end

    def insere_valor_da_proposta(valorproposta)
        sleep 2
        find("input[placeholder='Valor da proposta']").set "%.2f"% "#{valorproposta}"
    end

    def insere_um_valor(valorproposta)
        sleep 2
        find("input[placeholder='Insira um valor']").set "%.2f"% "#{valorproposta}"
    end

    def clica_lik_nao_cabe_meu_bolso
        clicar_nao_cabe_meu_bolso.click
    end
    # Em 09/09/2022 Refatorado sendo retirado esta chamada de classe que chama a clase busca_botao
    #               pois está sendo clicado diretamente no botão em 'poposta.rb' linha 28
    ##def informa_pagamento_a_vista(avista)
    ##    @classe_login.busca_botao(avista)
    ##end

    def seleciona_data(datapagato)
        #botao_proximo.click
        #find(:xpath, "//body/div[@id='app']/div[1]/main[1]/div[1]/div[1]/div[2]/section[1]/div[1]/div[1]/div[1]").click
        #ind(:xpath, "//*[@id='main']/div[1]/div/div[2]/div/section/div/div/button[1]").click
        sleep 5
        if datapagato == "1"
            seleciona_data_1.click
        elsif datapagato == "2"
            seleciona_data_2.click
        elsif datapagato == "3"
            seleciona_data_3.click
        elsif datapagato == "4"
            seleciona_data_4.click
        elsif datapagato == "5"
            seleciona_data_5
        end
    end

    def seleciona_qtd_parcelas(qtdparcelas)
        #binding.pry
        if qtdparcelas == "1"
            seleciona_parcela_1.click
        elsif qtdparcelas == "2"
            seleciona_parcela_2.click
        elsif qtdparcelas == "3"
            seleciona_parcela_3.click
        elsif qtdparcelas == "4"
            seleciona_parcela_4.click
        end

    end

    def detalhes_divida
        #binding.pry
        clicar_detalhes_divida.click
    end

    def mais_informacoes_divida
        clicar_mais_informacoes.click
    end

    # Em 09/09/2022 Refatorado sendo retirado esta chamada de classe que chama a clase busca_botao
    #               pois está sendo clicado diretamente no botão em 'poposta.rb' linha 44
    ##def simula_proposta(simularproposta)
    ##    @classe_login.busca_botao(simularproposta)
    ##end

    def clicar_botao_yes
        botao_yes.click
    end

    def clicar_botao_no
        botao_no.click
    end

    #def insere_valor_alto_a_vista(valoralto)
    #    sleep 3
    #    find(:xpath, "//input[@type='tel']").set "%.2f"% "#{valoralto}"
    #end

    def fechar_tela_x
        clicar_fechar_tela_x.click
    end

    def seta_voltar
        clicar_seta_voltar.click
    end

    #def insere_valor_baixo_a_prazo(vlrbaixoprazo, btnproximo)
    #    sleep 3
    #    find(:xpath, "//input[@type='tel']").set "%.2f"% "#{vlrbaixoprazo}"
    #    click_on(btnproximo)
    #end

    #def valida_primeira_parcela_valor_baixo(vlrbaixoprazonovo, btnproximo)
    #    page.has_text?(@valor_baixo_parcelado)
    #    click_on(btnproximo)
    #end

    # Método que preenche o valor da Proposta com um percentual do Valor Total da Dívida
    def preencer_valor_proposta(pervlr)
        inputText = find(:xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step1-proposal/section/p/span")
        valor = inputText.text
        valor = valor.gsub('R', '').gsub('$', '').gsub(' ', '').gsub('.', '').gsub(',', '.').to_f
        @valor_proposta = (valor * (pervlr).to_f).to_f
        find("input[placeholder='Valor da proposta']").set "%.2f"% "#{@valor_proposta}"
    end

    #def insere_valor_alto_a_prazo
    #    inputText = find(:xpath, "//html[1]/body[1]/div[1]/div[1]/main[1]/div[1]/div[1]/div[1]/section[1]/p[1]/span[2]")
    #    valor = inputText.text
    #    valor = valor.gsub('R', '').gsub('$', '').gsub(' ', '').gsub('.', '').gsub(',', '.').to_f
    #    @valor_alto_parcelado = ((valor/2)/2).to_f
    #    length = inputText.text.length
    #    keys = Array.new(length, :backspace)
    #    find(:xpath, "//input[@type='tel']").send_keys keys, "%.2f"% "#{@valor_alto_parcelado}"
    #end

    #def valida_primeira_parcela_valor_alto
    #    botao_proximo.click
    #    page.has_text?(@valor_alto_parcelado)
    #    botao_proximo.click
    #end

    #def pega_valor_total
    #    inputText = find(:xpath, "//p[@class='valor-proposta']")
    #    @valor = inputText.text
    #    @valor_total = @valor.gsub('R', '').gsub('$', '').gsub(' ', '').gsub('.', '').gsub(',', '.').to_f
    #end

    #def altera_valor_total_a_vista_para_cima
    #    length = @valor.length
    #    keys = Array.new(length, :backspace)
    #    @valor_total_a_vista_para_cima = (@valor_total*80).to_f
    #    find(:xpath, "//input[@name='valorTotal']").send_keys keys, "%.2f"% "#{@valor_total_a_vista_para_cima}"
    #end

    #def altera_valor_parcela_unica_para_cima
    #    length = @valor.length
    #    keys = Array.new(length, :backspace)
    #    find(:xpath, "//input[@name='valorEntrada']").send_keys keys, "%.2f"% "#{@valor_total_a_vista_para_cima}"
    #    find(:xpath, "//span[@class='valor-total-divida__label']").click
    #end

    #def altera_valor_total_a_vista_para_baixo
    #    length = @valor.length
    #    keys = Array.new(length, :backspace)
    #    @valor_total_a_vista_para_baixo = (@valor_total*0.02).to_f
    #    find(:xpath, "//input[@name='valorTotal']").send_keys keys, "%.2f"% "#{@valor_total_a_vista_para_baixo}"
    #end

    #def envia_proposta(enviar)
    #    @classe_login.busca_botao(enviar)
    #end

    # Código que pega o valor do campo e calcula um percentual
    ###inputText = find(:xpath, "//html[1]/body[1]/div[1]/div[1]/main[1]/div[1]/div[1]/div[1]/section[1]/p[1]/span[2]")
    ###valor = inputText.text
    ###valor = valor.gsub('R', '').gsub('$', '').gsub(' ', '').gsub('.', '').gsub(',', '.').to_f
    ###@valor_baixo_parcelado = ((valor*0.014)/2).to_f
    ###length = inputText.text.length
    ###keys = Array.new(length, :backspace)
    ###find(:xpath, "//input[@type='tel']").send_keys keys, "%.2f"% "#{@valor_baixo_parcelado}"

        ###def insere_valor_baixo_a_vista(valorbaixo)
    ###    ##inputText = find(:xpath, "//html[1]/body[1]/div[1]/div[1]/main[1]/div[1]/div[1]/div[1]/section[1]/p[1]/span[2]")
    ###    ##valor = inputText.text
    ###    ##valor = valor.gsub('R', '').gsub('$', '').gsub(' ', '').gsub('.', '').gsub(',', '.').to_f
    ###    ##valor_baixo = (valor*0.01).to_f
    ###    ##length = inputText.text.length
    ###    ##keys = Array.new(length, :backspace)
    ###    ##find(:xpath, "//input[@type='tel']").send_keys keys, "%.2f"% "#{valor_baixo}"
    ###    sleep 2
    ###    #find(:xpath, "//input[@type='tel']").set "%.2f"% "#{valorbaixo}"
    ###    find("input[placeholder='Valor da proposta']").set "%.2f"% "#{valorbaixo}"
    ###end

end