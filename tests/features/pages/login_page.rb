class LoginPage < SitePrism::Page

    set_url '/'

    element :login_user, '#username'
    element :login_password, '#password'
    element :login_button, '#kc-login'

    def faz_login(cpf, senha)
        login_user.set cpf
        login_password.set senha
        login_button.click
    end

    def valida_login(mensagem)
        page.has_text?(mensagem)
    end

    def busca_botao(botao)
        first('button', :text => botao).click
    end

    def faz_logout(sair)
        busca_botao(sair)
    end

end