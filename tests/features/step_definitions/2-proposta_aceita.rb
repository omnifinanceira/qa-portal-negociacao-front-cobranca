Dado('eu clico no botao detalhes da divida') do
    @proposta.detalhes_divida
    #find(:xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-detail-debit-negotiated/section/div/div[1]/div[2]/p").click
end

Dado('eu clico no botao mais informacoes') do
    #binding.pry
    sleep 10
    @proposta.mais_informacoes_divida
end

Entao('devo validar que estou na tela de mais informacoes') do
    expect(@proposta).to have_texto_mais_informacoes
end

Entao('devo validar que estou na tela precisa de ajuda') do
    expect(@proposta).to have_texto_precisa_ajuda
end

Entao('devo validar que estou na tela mais informacoes') do
    #binding.pry
    #expect(@proposta).to have_texto_parcelas_em_atraso
    expect(page.assert_text('Parcelas em atraso')).to eq true
end

# Clica no botão "x" para fechar a tela de Mais Informações
Quando('eu clico no botao x para fechar a tela') do
    #binding.pry
    @proposta.fechar_tela_x
    #find(:xpath, "//*[@id='cdk-overlay-1']/app-debt-details-more-info/div/div[1]").click
end

Quando('eu clico no botao seta esquerda para tela anterior') do
    #binding.pry
    @proposta.seta_voltar
    #find(:xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/app-navbar-negotiation-web/div/div").click
end

##Quando('eu clico no botao seta esquerda para voltar aos cards da divida') do
##    binding.pry
##    @proposta.seta_voltar
##    #find(:xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/app-navbar-negotiation-web/div/div").click
##end

Entao('valido que que estou na tela dos cards {string}') do |opertp|
    #binding.pry
    #expect(@proposta).to have_texto_divida_negociada
    if page.has_text?('Dívida negociada')
        #sleep 10
        tirar_foto_allure("PrjPortalNegociações-TelaCards-#{opertp}","TESTADO")
    elsif page.has_text?('Dívida')
        tirar_foto_allure("PrjPortalNegociações-TelaCards-#{opertp}","TESTADO")
    end
end
