# Carrega os cardas das dívidas do cliente
Dado('que eu espere as dívidas serem carregadas') do
    @proposta = PropostaPage.new
    @proposta.aguarda_dividas
    #binding.pry
end

### Em 09/09/2022: Refatoração. Retirado este código (Acerto)
###Então('devo clicar no botão {string}') do |simular|
###
###    @proposta.simula(simular)
###end

# Clica no card da dívida do cliente
Dado('eu clico no card da divida do cliente') do
    #page.driver.browser.navigate.refresh
    #binding.pry
    find(:xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-home-negotiation/div/section/omnids-card-debt-pn01/div/omni-card/div/div[2]/div/div[1]").click
end

# Preenche o campo do valor da Proposta
# Este código foi substituído pelo próximo Então
Então('preencher o campo com um valor da proposta {string}') do |valorproposta|
    #@proposta.insere_valor_baixo_a_vista(valorproposta)
    @proposta.insere_valor_da_proposta(valorproposta)
end

# Envia valor percentual para calcular sobre o Valor Atual da Dívida
Então('eu preencho o campo com um valor de proposta {string}') do |pervlr|
    @proposta.preencer_valor_proposta(pervlr)
end


Dado('eu preencho o campo insira um valor {string}') do |valorproposta|
    @proposta.insere_um_valor(valorproposta)
end

# Clica no botão
Quando('eu clico no botao {string}') do |btnavanca|
    #binding.pry
    tirar_foto("PortalNegociação-","TESTADO")
    click_on(btnavanca)
end

# Clica na Forma da Pagamento - À Vista ou Parcelado
Então('eu devo informar a forma {string} de pagamento {string}') do |btnformapg, opertp|
    # Clica nos botões da tela Forma de Pagamento: "À vista" ou "Parcelado"
    #binding.pry
    click_on(btnformapg)
    tirar_foto_allure("PrjPortalNegociações-PgAVista-#{opertp}","TESTADO")
    # Em 09/09/2022 Linha abaixo substituída pois chama um método que chama outro método. Simplificamos
    #@proposta.informa_pagamento_a_vista(avista)
end

# Validar a escolha do pagamento À Vista. Confirma texto na tela exibida
Quando('eu validar a escolha de pagamento a vista clicando no card com valor') do
    expect(@proposta).to have_texto_confirmacao_a_vista
    find(:xpath, "html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step2-installment/section/div/ul/li/omnids-installment-value01/div/div").click
end

# Clica em uma Data de Pagamento dentro das cinco exibidas na tela
Então('devo selecionar uma data {string}') do |datapagato|
    @proposta.seleciona_data(datapagato)
    #@proposta.pega_valor_total
end

# Clica no botão "Simular proposta"
Quando('eu clicar em {string}') do |simularproposta|
    @proposta.aguarda_detalhes_divida
    click_on(simularproposta)
    # Em 09/09/2022 Linha abaixo substituída pois chama um método que chama outro método. Simplificamos
    #@proposta.simula_proposta(simularproposta)
end

Quando('eu validar a escolha de pagamento parcelado clicando no card com valor {string}') do |qtdparcelas|
    expect(@proposta).to have_texto_confirmacao_parcelado
    @proposta.seleciona_qtd_parcelas(qtdparcelas)
end

# Valida o texto exibido na tela antes de clica no botão "Enviar proposta"
Entao('devo validar o resumo da proposta {string}') do |opertp|
    sleep(10)
    expect(@proposta).to have_texto_resumo_proposta
    tirar_foto_allure("PrjPortalNegociações-PROPOSTA-#{opertp}","TESTADO")
    #binding.pry
    ## Pega os valores exibidos na tela final da proposta
    # Pega o valor da Dívida Original
    #divida_original = find(:xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step4-resume/section/div/p/span").text
    # Transforma o conteúdo da variável em float e retira o R$ e a virgula
    #divida_original = divida_original.gsub('R', '').gsub('$', '').gsub(' ', '').gsub('.', '').gsub(',', '.').to_f
    #valor_proposta = find(:xpath, "/html/body/app-root/app-main-negotiation-web-layout/main/section/div/div/app-step4-resume/section/div/div/div[1]/div/p").text
    #valor_proposta = valor_proposta.gsub('R', '').gsub('$', '').gsub(' ', '').gsub('.', '').gsub(',', '.').to_f
    # Acha o percentual do valor pago sobre a dívida, arredondando para 2 casas decimais.
    #vlr_resultado = ((valor_proposta/divida_original)*100).ceil(2)
end

Entao('eu clico no botao enviar proposta {string}') do |btnavanca|
    click_on(btnavanca)
end

Entao ('eu verifico msg envio da proposta {string}') do |opertp|
    sleep 20
    binding.pry
    if page.has_text?('Muito bem! Agora nós temos um acordo')
        tirar_foto_allure("PrjPortalNegociações-PRO ENVIADA-#{opertp}","TESTADO")
        click_on('Entendi')
    elsif page.has_text?('Ocorreu um erro')
        tirar_foto_allure("PrjPortalNegociações-MSG_ERRO DEU_CERTO-#{opertp}","TESTADO")
        click_on('Fechar')
    end
end

Entao('verifico mensagem de nao divida clicando no botao {string} e {string}') do |btnavanca, opertp|
    expect(@proposta).to have_texto_sem_divida_atraso
    tirar_foto_allure("PrjPortalNegociações-SEM DIVIDA-#{opertp}","TESTADO")
    click_on(btnavanca)
end

Entao('verifico se tem proposta pre aprovada no cpf {string}') do |opertp| 
    @proposta.aguarda_detalhes_divida
    # Comando has_text? pergunta se o elemento existe na tela e retorn true ou false
    # Comando if retirado pois não existem mais propostas pré aprovadas
    ##if page.has_text?('Nossa proposta:')
    ##    sleep 10
    ##    tirar_foto_allure("PrjPortalNegociações-PRÉ_SIM-#{opertp}","TESTADO")
    if page.has_text?('Renegocie agora')
        tirar_foto_allure("PrjPortalNegociações-PRÉ_NÃO-#{opertp}","TESTADO")
    elsif page.has_text?('Dívida renegociada')
        tirar_foto_allure("PrjPortalNegociações-ACORDO-#{opertp}","TESTADO")
    elsif page.has_text?('Importante')   
        tirar_foto_allure("PrjPortalNegociações-IMPORTANTE-#{opertp}","TESTADO")
    elsif page.has_text?('Propósta em análise')
        tirar_foto_allure("PrjPortalNegociações-PROP_ANALISE-#{opertp}","TESTADO")
    end
end
#page.has_field?('elemento')
#expect(page.has_field?('elemento')).to eq true
###end
Dado('eu clico no botao yes {string}') do |opertp|
    @proposta.aguarda_proposta_pre_aprovada
    expect(page.assert_text('Nossa proposta:')).to eq true
    tirar_foto_allure("PrjPortalNegociações-YES-#{opertp}","TESTADO")
    @proposta.clicar_botao_yes
end

Entao('devo validar o resumo da proposta pre aprovada {string}') do |opertp|
    expect(@proposta).to have_texto_resumo_acordo
    expect(page.assert_text('Enviar proposta')).to eq true
    tirar_foto_allure("PrjPortalNegociações-ACORDO-#{opertp}","TESTADO")
end

Dado('eu clico no botao nao {string}') do |opertp|
    @proposta.aguarda_proposta_pre_aprovada
    expect(page.assert_text('Nossa proposta:')).to eq true
    tirar_foto_allure("PrjPortalNegociações-NO-#{opertp}","TESTADO")
    @proposta.clicar_botao_no
    #binding.pry
end

Dado('eu clico no link nao cabe no meu bolso') do
    sleep 10
    @proposta.clica_lik_nao_cabe_meu_bolso
    #expect(page.assert_text('Nossa proposta:')).to eq true
    #@proposta.clicar_botao_no
    #binding.pry
end

Dado('eu clico na opcao exibir boleto') do
    find('h5', text:"Exibir").click
end
  
Entao('verifico a exibicado do boleto gerado {string}') do |opertp|
    sleep 5
    #binding.pry
    page_ultima = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(page_ultima)
    tirar_foto_allure("PrjPortalNegociações-BOLETO_GERADO-#{opertp}","TESTADO")
end