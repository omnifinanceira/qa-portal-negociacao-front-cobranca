Dado('que eu estou na página de login do Negocie Omni') do
    @login = LoginPage.new
    @login.load
    #binding.pry
    #find('a', text:"Negociar agora").click
end

Quando('eu faço login com {string} e {string}') do |cpf, senha|
    #binding.pry
    @login.load
    #find('a', text:"Negociar agora").click
    find('a', text:"Saiba mais").click
    @login.faz_login(cpf, senha)
end

Então('devo ver a mensagem {string} no dashboard') do |mensagem|
    @login.valida_login(mensagem)
end

Quando('faço login com {string} e {string}') do |cpf, senha|
    @login.load
    #find('a', text:"Negociar agora").click
    find('a', text:"Saiba mais").click
    @login.faz_login(cpf, senha)
end

Então('devo verificar se a mensagem {string} será exibida') do |mensagem|
    alert = find(".input-text-error--active")
    expect(alert.text).to eql mensagem
end

Então('devo verificar se o alerta {string} será exibido') do |mensagem|
    alert = find(".kc-feedback-text")
    expect(alert.text).to eql mensagem
  end

Quando('clico em {string}') do |sair|
    @login.faz_logout(sair)
end

Então('devo verificar se sou redirecionado para a tela de login') do
    expect(@login).to have_login_user
end