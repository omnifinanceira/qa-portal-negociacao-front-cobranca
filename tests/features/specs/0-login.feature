#language: pt

Funcionalidade: Acessar o sistema Negocie Omni
- Para que eu possa acessar o sistema sendo um usuário
- Para que eu possa validar os campos de login com dados incongruentes
- E possa realizar logout da minha conta

Contexto: Logado
Dado que eu estou na página de login do Negocie Omni

@login_proposta_sucesso
Cenário: Logar no sistema Negocie Omni
    #Quando eu faço login com "379.417.598-02" e "Senha123"
    Quando eu faço login com <cpf> e <senha>
    Dado que eu espere as dívidas serem carregadas
    Então devo ver a mensagem "Renegociar dívidas" no dashboard

Exemplos:
    |cpf             |senha      |
    |"379.417.598-02"|"Senha123" |
    #|"340.172.245-04"|"SENHA@123"| SEM DIVIDA
    #|"724.828.936-15"|"SENHA@123"|
    #|"602.420.781-68"|"4ET36G$JZ"|

@validacao_login_proposta_01
Cenário: Validacao de campos com dados incongruentes
    Quando faço login com " " e "123456"
    Então devo verificar se a mensagem "Este campo é obrigatório" será exibida

@validacao_login_proposta_02
Cenário: Validacao de campos com dados incongruentes
    Quando faço login com "594.448.710-02" e "SENHA"
    Então devo verificar se o alerta "CPF ou senha inválida." será exibido

@logout_proposta
Cenário: Realizando logout
Quando eu faço login com "379.417.598-02" e "Senha123"
E clico em "Sair"
Então devo verificar se sou redirecionado para a tela de login