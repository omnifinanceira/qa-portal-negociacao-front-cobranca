#language: pt

@1
Funcionalidade: Simular propostas pelo Negocie Omni
- Para que eu possa acessar o sistema sendo um usuário/agente
- E possa simular propostas
- E possa validar se essas propostas tem probabilidade de serem aceitas

Contexto: Pré-requisitos para acessar o Negocie Omni
    * que eu estou na página de login do Negocie Omni
    # Esse contrato tem somente um contrato de dívida
    #* faço login com "138.178.538-74" e "SENHA@123"

### SIMULA A VISTA SEM PRÉ APROVADA >>>
### CENÁRIO QUE EFETUA A SIMULAÇÃO DE PROPOSTA À VISTA PARA UM CPF QUE NÃO POSSUE 
### PROPOSTA PRÉ-APROVADA PELA OMNI. NÃO ENVIA PROPOSTA
### ÚLTIMO TESTE RODADO E FUNCIONANDO EM 13/01/2023
@simulando_proposta_vista
Cenário: Simulando proposta à vista
    Quando faço login com <cpf> e <senha>
    Dado que eu espere as dívidas serem carregadas
    E eu clico no card da divida do cliente
    Quando eu clicar em 'Simular proposta'
    E eu preencho o campo com um valor de proposta <pervlr>
    E eu clico no botao 'Continuar'
    Então eu devo informar a forma 'À vista' de pagamento 'PropostaPortal-FPag-AVista' 
    Quando eu validar a escolha de pagamento a vista clicando no card com valor
    E devo selecionar uma data <datapagato> 
    E eu clico no botao 'Continuar'
    Entao devo validar o resumo da proposta 'PropostaPortal-AVista'

Exemplos:
    |cpf             |senha      |valorproposta|datapagato|pervlr|
    |"847.111.838-68"|"SENHA@123"|"1000"       |"1"       |"0.10"|

#========================= FUNCIONALIDADE DESCONTINUADA =========================#
# Em Janeiro de 23 foi definido que não haverá mais proposta pré aprovada,
# Por isso esse código deixou de ser utilizado
#>> SIMULA A VISTA COM PRÉ APROVADA >>>
#>> CENÁRIO QUE EFETUA A SIMULAÇÃO DE PROPOSTA À VISTA PARA UM CPF QUE POSSUE 
#>> PROPOSTA PRÉ-APROVADA PELA OMNI. NÃO ENVIA PROPOSTA
#>> ÚLTIMA EXECUÇÃO DE TESTE E FUNCIONANDO EM 14/11/2022
#>>@simulando_proposta_vista_com_prop_pre
#>>Cenário: Simulando proposta à vista
#>>    Quando faço login com <cpf> e <senha>
#>>    Dado que eu espere as dívidas serem carregadas
#>>    E eu clico no card da divida do cliente
#>>    E eu clico no botao yes 'PropostaPortalPré-Aprov-Yes'
#>>    Entao devo validar o resumo da proposta pre aprovada 'PropostaPortalPré-Aprov-AVista'
#>>
#>>Exemplos:
#>>    |cpf             |senha      |valorproposta|datapagato|
#>>    |"298.240.068-50"|"SENHA@123"|"1000"       |"1"       |
#================================================================================#

#========================= FUNCIONALIDADE DESCONTINUADA =========================#
# Em Janeiro de 23 foi definido que não haverá mais proposta pré aprovada,
# Por isso esse código deixou de ser utilizado
#>> SIMULA CPF COM PROPOSTA PRÉ APROVADA >>>
#>> CENÁRIO QUE EFETUA A SIMULAÇÃO DE PROPOSTA PRÉ APROVADA PARA UM CPF 
#>> CLICA NO BOTÃO NÃO PARA ACEITE DA PROPOSTA À VISTA - FLUXO DO NÃO
#>> ÚLTIMO TESTE RODADO E FUNCIONANDO EM 14/11/2022
#>>@simula_proposta_preaprov_nao_aceita
#>>Cenário: Simulando proposta pre aprovada fluxo do nao
#>>    Quando faço login com <cpf> e <senha>
#>>    Dado que eu espere as dívidas serem carregadas
#>>    E eu clico no card da divida do cliente
#>>    E eu clico no botao nao 'PropostaPortalPré-Aprov-No'
#>>    E eu preencho o campo insira um valor <valorproposta>
#>>    E eu clico no botao 'Continuar'
#>>    E eu clico no link nao cabe no meu bolso
#>>    E eu clico no botao 'Não tenho interesse'
#>>    Quando eu clicar em 'Simular proposta'
#>>    E eu preencho o campo com um valor de proposta <pervlr>
#>>    E eu clico no botao 'Continuar'
#>>    Então eu devo informar a forma 'À vista' de pagamento 'PropostaPortalPréAprov-FPag-AVista'
#>>    Quando eu validar a escolha de pagamento a vista clicando no card com valor
#>>    E devo selecionar uma data <datapagato> 
#>>    E eu clico no botao 'Continuar'
#>>    Entao devo validar o resumo da proposta 'PropostaPortalPréAprov-AVista'
#>>
#>>Exemplos:
#>>    |cpf             |senha      |valorproposta|datapagato|pervlr|
#>>    |"298.240.068-50"|"SENHA@123"|"1000"       |"1"       |"0.10"|
#================================================================================#

### SIMULA A PRAZO >>> 
### CENÁRIO QUE EFETUA A SIMULAÇÃO DE PROPOSTA A PRAZO. NÃO ENVIA PROPOSTA
### ÚLTIMO TESTE RODADO E FUNCIONANDO EM 13/01/2023 
@simulando_proposta_prazo
Cenário: Simulando proposta parcelado
    Quando faço login com <cpf> e <senha>
    Dado que eu espere as dívidas serem carregadas
    E eu clico no card da divida do cliente
    Quando eu clicar em 'Simular proposta'
    E eu preencho o campo com um valor de proposta <pervlr>
    E eu clico no botao 'Continuar'
    Então eu devo informar a forma 'Parcelado' de pagamento 'PropostaPortal-FPag-Parcelado'
    Quando eu validar a escolha de pagamento parcelado clicando no card com valor <qtdparcelas>
    E devo selecionar uma data <datapagato> 
    E eu clico no botao 'Continuar'
    Entao devo validar o resumo da proposta 'PropostaPortal-Parcelado'

Exemplos:
    |cpf             |senha      |valorproposta|datapagato|qtdparcelas|pervlr|
    |"373.268.708-29"|"SENHA@123"|"500"        |"2"       |"2"        |"0.20"|

### ENVIAR PROPOSTA >>> CENÁRIO QUE EFETUA O ENVIO DE PROPOSTA DE ACORDO PARA A OMNI 
### ÚLTIMO TESTE RODADO E FUNCIONANDO EM 13/01/2023 
@simula_enviar_proposta_vista
Cenário: Simula e envia proposta à vista para a Omni
    Quando faço login com <cpf> e <senha>
    Dado que eu espere as dívidas serem carregadas
    E eu clico no card da divida do cliente
    Quando eu clicar em 'Simular proposta'
    E eu preencho o campo com um valor de proposta <pervlr>
    E eu clico no botao 'Continuar'
    Então eu devo informar a forma 'À vista' de pagamento 'PropostaPortal-Envia-AVista'
    Quando eu validar a escolha de pagamento a vista clicando no card com valor
    E devo selecionar uma data <datapagato> 
    E eu clico no botao 'Continuar'
    Entao devo validar o resumo da proposta 'PropostaPortal-AVista'
    E eu clico no botao enviar proposta 'Enviar proposta'
    Entao eu verifico msg envio da proposta 'PropostaPortal-AVista-Enviada'

Exemplos:
    |cpf             |senha      |valorproposta|datapagato|pervlr|
    |"225.592.008-56"|"SENHA@123"|"1000"       |"1"       |"0.50"|

# Este CPF tem proposta pré aprovada e usa a nova versão do Portal de Negociação
### CLIENTE SEM DÍVIDA >>> CENÁRIO QUE EFETUA O TESTE COM UM CLIENTE QUE NÃO TEM DÍVIDA EM ATRASO NA OMNI
### ÚLTIMO TESTE RODADO E FUNCIONANDO EM 21/11/2022
@simulando_cliente_sem_divida
Cenário: Simulando cliente sem divida na omni
    Quando faço login com <cpf> e <senha>
    Dado que eu espere as dívidas serem carregadas
    Entao verifico mensagem de nao divida clicando no botao 'Entendi' e 'PropostaPortal-SemDivida'

Exemplos:
    |cpf             |senha      |
    |"340.172.245-04"|"SENHA@123"|

### CLICA EM GERAR BOLETO DIVIDA RENEGOCIADA>>>
### CENÁRIO QUE EFETUA A GERAÇÃO DO BOLETO PARA DÍVIDA RENEGOCIADA
### ÚLTIMO TESTE RODADO E FUNCIONANDO EM: 23/11/2022
@gera_boleto_renegociacao
Cenário: Geracao do boleto divida renegociada
    Quando faço login com <cpf> e <senha>
    Dado que eu espere as dívidas serem carregadas
    E eu clico no card da divida do cliente
    E eu clico no botao 'Gerar boleto'
    E eu clico no botao 'Entendi'
    E eu clico na opcao exibir boleto
    Entao verifico a exibicado do boleto gerado 'PropostaPortal-BoletoGerado'

Exemplos:
    |cpf             |senha      |pre|planilha|
    |"724.828.936-15"|"SENHA@123"|"R"|"Sem Pl"|

### CONSULTA STATUS DE DÍVIDA DOS CLIENTES >>>
### CENÁRIO QUE EFETUA A CONSULTA DO STATUS DA DÍVIDA DOS CLIENTES
### DISTINGUINDO A DIVIDA QUE TEM PROPOSTA PRÉ APROVADA, SEM PRÉ APROVADA E DÍVIDA JÁ RENEGOCIADA 
### E OUTROS STATUS
### ÚLTIMO TESTE RODADO E FUNCIONANDO EM: 21/11/2022
@consulta_divida_cliente
Cenário: Consulta divida do cliente
    Quando faço login com <cpf> e <senha>
    Dado que eu espere as dívidas serem carregadas
    E eu clico no card da divida do cliente
    # Quando eu clicar em 'Simular proposta'   # Dívida Sem Pré Aprovado
    #Entao verifico se tem proposta pre <preaprov> aprovada <titulopage> no cpf 'PropostalPortal'
    Entao verifico se tem proposta pre aprovada no cpf 'PropostaPortal'

Exemplos:
    |cpf             |senha      |pre|planilha|
    |"724.828.936-15"|"SENHA@123"|"R"|"Sem Pl"|
    |"481.852.518-94"|"SENHA@123"|"S"|"335-2" |
    |"051.777.691-00"|"SENHA@123"|"P"|"335-2" |
    #|"080.365.088-42"|"SENHA@123"|"S"|"335-3" |
    |"327.343.488-02"|"SENHA@123"|"I"|"335-3" |
    #|"225.592.008-56"|"SENHA@123"|"S"|"335-3" |
    #|"298.240.068-50"|"SENHA@123"|"S"|"335-3" |
    #|"063.227.848-03"|"SENHA@123"|"S"|"335-3" |
    #|"153.878.728-89"|"SENHA@123"|"S"|"335-3" |
    #|"402.194.048-01"|"SENHA@123"|"S"|"335-3" |
    #|"339.350.648-02"|"SENHA@123"|"S"|"335-3" |
    #|"054.681.908-77"|"SENHA@123"|"S"|"335-3" |
    #|"420.742.228-58"|"SENHA@123"|"S"|"335-3" |
    #|"138.306.718-06"|"SENHA@123"|"S"|"335-3" |
    #|"415.750.038-56"|"SENHA@123"|"S"|"335-3" |
    #|"383.261.778-73"|"SENHA@123"|"R"|"335-3" |
    #|"373.268.708-29"|"SENHA@123"|"S"|"335-3" |
    #|"002.404.178-52"|"NOVA@2022"|"S"|"335-3" |
    #|"069.659.268-13"|"NOVA@2022"|"S"|"335-3" |
    #|"065.113.968-60"|"DIVIDA@22"|"S"|"335-3" |
    #|"478.428.348-06"|"NOVA@2022"|"S"|"335-3" |
    #|"432.166.208-75"|"DIVIDA@22"|"S"|"335-3" |
    #|"021.145.331-58"|"SENHA@123"|"S"|"Sem Pl"|
    #|"424.374.878-08"|"SENHA@123"|"S"|"335-2" |
    #|"392.819.808-43"|"SENHA@123"|"S"|"335-2" |
    #|"847.111.838-68"|"SENHA@123"|"S"|"335-2" |
    #|"358.145.038-07"|"SENHA@123"|"S"|"335-2" |
    #|"366.112.098-08"|"SENHA@123"|"S"|"335-2" | 
    #|"069.792.408-46"|"SENHA@123"|"N"|"335-2" |
    #|"485.014.228-19"|"SENHA@123"|"S"|"335-2" |
    #|"351.224.058-59"|"SENHA@123"|"S"|"335-2" |
    #|"141.936.278-02"|"SENHA@123"|"S"|"335-2" |
    #|"080.304.118-71"|"SENHA@123"|"S"|"335-2" |
    #|"416.333.738-51"|"SENHA@123"|"S"|"335-2" |
    #|"413.101.418-19"|"SENHA@123"|"S"|"335-2" |
    #|"138.178.538-74"|"SENHA@123"|"S"|"335-2" |
    #|"120.240.478-23"|"SENHA@123"|"S"|"335-2" |
    #|"320.065.998-01"|"SENHA@123"|"R"|"335-2" | 
    #|"340.172.245-04"|"SENHA@123"|"SD"|"00"   | 
    ##|"334.446.518-05"|"SENHA@123"|"2C"|"335-2" |
    ##|"289.674.858-08"|"SENHA@123"|"2C"|"335-2" |
