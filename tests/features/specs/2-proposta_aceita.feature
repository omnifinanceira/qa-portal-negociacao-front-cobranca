#language: pt

@2
Funcionalidade: Consultar fluxo divida negociada no portal Cobranca Omni
- Para que eu possa acessar o sistema sendo um usuário/agente
- E possa consultar uma divida negociada
- E possa validar o fluxo da divida negociada

Contexto: Pré-requisitos para acessar o Portal de Cobranca Omni
    * que eu estou na página de login do Negocie Omni
    # Esse cpf tem uma dívida já negociada no Portal de Cobrança Omni
    #* faço login com "347.090.178-32" e "V0963&9Q"

@consulta_divida_negociada
Cenário: Consultando fluxo detalhes da divida
    Quando faço login com <cpf> e <senha>
    Dado que eu espere as dívidas serem carregadas
    E eu clico no card da divida do cliente
    ##E eu clico no botao detalhes da divida
    E eu clico no botao mais informacoes
    Entao devo validar que estou na tela de mais informacoes
    
Exemplos:
    |cpf             |senha      |
    |"847.111.838-68"|"SENHA@123"|

@consulta_divida_negociada_ajuda
Cenário: Consultando fluxo detalhes da divida
    Quando faço login com <cpf> e <senha>
    Dado que eu espere as dívidas serem carregadas
    E eu clico no card da divida do cliente
    ##E eu clico no botao detalhes da divida
    E eu clico no botao mais informacoes
    E eu clico no botao 'Continuar'
    Entao devo validar que estou na tela precisa de ajuda

Exemplos:
    |cpf             |senha      |
    |"847.111.838-68"|"SENHA@123"|

@consulta_divida_negociada_botoes_voltar
Cenário: Consultando fluxo detalhes da divida - botoes voltar
    Quando faço login com <cpf> e <senha>
    Dado que eu espere as dívidas serem carregadas
    E eu clico no card da divida do cliente
    ##E eu clico no botao detalhes da divida
    E eu clico no botao mais informacoes
    Entao devo validar que estou na tela mais informacoes
    Quando eu clico no botao x para fechar a tela
    E eu clico no botao seta esquerda para tela anterior
    ##E eu clico no botao seta esquerda para voltar aos cards da divida
    Dado que eu espere as dívidas serem carregadas
    Entao valido que que estou na tela dos cards 'PropostaPortal-Cards'

Exemplos:
    |cpf             |senha      |
    |"847.111.838-68"|"SENHA@123"|